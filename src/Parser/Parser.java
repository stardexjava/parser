package Parser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;

public class Parser {

    private HashMap<String, String> parseMap = new HashMap<>();

    private String[] keys = new String[]{
            "InjectorCode", "Manufacturer", "Model", "PulseFileName",
            "CodeType", "Calibration_Id", "Checksum_M", "CodingWithoutInitialCode",
            "NumberOfCodeChars", "K_Coefficient",

            "TEST_NAME", "MotorSpeed", "SettedPressure", "CodeField",
            "InjectionRate", "TotalPulseTime", "NominalFlow",
            "FlowRange", "FlowDivisor"
    };

    public Parser() {
        resetInjectorMap();
        resetInjectorTestMap();
    }

    private void resetInjectorMap() {
        parseMap.put(keys[0], null);
        parseMap.put(keys[1], null);
        parseMap.put(keys[2], null);
        parseMap.put(keys[3], null);
        parseMap.put(keys[4], null);
        parseMap.put(keys[5], null);
        parseMap.put(keys[6], null);
        parseMap.put(keys[7], null);
        parseMap.put(keys[8], null);
        parseMap.put(keys[9], null);
    }

    private void resetInjectorTestMap() {
        parseMap.put(keys[10], null);
        parseMap.put(keys[11], null);
        parseMap.put(keys[12], null);
        parseMap.put(keys[13], null);
        parseMap.put(keys[14], null);
        parseMap.put(keys[15], null);
        parseMap.put(keys[16], null);
        parseMap.put(keys[17], null);
        parseMap.put(keys[18], null);
    }

    private FileWriter writerInjector;
    private FileWriter writerinjectorTests;

    private int currentTestId = 1;


    public void parse() {
        String from = "/home/trein/Documents/Target";
        String to = "/home/trein/Documents/AfterParsing";
        parse(from, to);
    }

    public void parse(String pathFrom, String pathTo) {
        File fileFrom = new File(pathFrom);
        try {
            writerInjector = new FileWriter(pathTo + File.separator + "injectors.csv", true);
            writerinjectorTests = new FileWriter(pathTo + File.separator + "injector_tests.csv", true);

            writerInjector.write("injector_code,manufacturer,injector_type,volt_ampere_profile,codetype," +
                    "calibration_id,checksum_m,coding_without_initial_code,number_of_code_chars,k_coefficient");
            writerInjector.write('\n');

            writerinjectorTests.write("id,injector_code,test_name,motor_speed,setted_pressure,adjusting_time,measurement_time,codefield," +
                    "injection_rate,total_pulse_time,nominal_flow,flow_range,Flow_Divisor");
            writerinjectorTests.write('\n');
        } catch (IOException e) {
            e.printStackTrace();
        }


        readFile(fileFrom);
        finish();
    }

    private void readFile(File file) {
        System.err.println(file.getAbsolutePath());

        File[] files = file.listFiles();

        if (files == null)
            return;

        for (File file1 : files) {
            if (file1.isDirectory()) {
                readFile(file1);
            } else {
                try {
                    Files.lines(file1.toPath(), StandardCharsets.UTF_8).forEach(this::parseLine);
                    endOfFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void parseLine(String s) {
        System.err.println(s);

        String[] splitStr = s.split(":");

        if (splitStr.length < 2)
            return;

        if (splitStr[0].equalsIgnoreCase(keys[10]) && parseMap.get(keys[10]) != null)
            endOfTest();


         if (splitStr[0].equalsIgnoreCase(keys[18])) {
            parseMap.put(keys[18], splitStr[1].trim() + "/" + splitStr[2].trim());
        } else {
            parseMap.put(splitStr[0], splitStr[1]);
        }
    }

    private void endOfTest() {
        try {
            writerinjectorTests.write(currentTestId++
                    + "," + parseMap.get(keys[0])
                    + "," + parseMap.get(keys[10])
                    + "," + parseMap.get(keys[11])
                    + "," + parseMap.get(keys[12])
                    + ",null"
                    + ",null"
                    + "," + parseMap.get(keys[13])
                    + "," + parseMap.get(keys[14])
                    + "," + parseMap.get(keys[15])
                    + "," + parseMap.get(keys[16])
                    + "," + parseMap.get(keys[17])
                    + "," + parseMap.get(keys[18])
            );

            writerinjectorTests.write('\n');

            resetInjectorTestMap();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void endOfFile() {
        try {
            writerInjector.write(
                    parseMap.get(keys[0])
                            + "," + parseMap.get(keys[1])
                            + "," + parseMap.get(keys[2])
                            + "," + parseMap.get(keys[3])
                            + "," + parseMap.get(keys[4])
                            + "," + parseMap.get(keys[5])
                            + "," + parseMap.get(keys[6])
                            + "," + parseMap.get(keys[7])
                            + "," + parseMap.get(keys[8])
                            + "," + parseMap.get(keys[9])
            );

            writerInjector.write('\n');

            resetInjectorMap();
            resetInjectorTestMap();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void finish() {
        try {
            writerInjector.flush();
            writerinjectorTests.flush();
            writerInjector.close();
            writerinjectorTests.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
